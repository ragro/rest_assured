package tests;
import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

import files.ReusableMethod;

public class Basics4{
	@Test
	public void postAnddeleteData() throws IOException {
		
		String bodyData = GenerateStringFromResponse("//home//rohit//Documents//Code//Eclipse//RestAssured//src//files//postData.xml");

		//Task1 (add a place)
		RestAssured.baseURI = "https://maps.googleapis.com";
			
			Response res = given().queryParam("key","AIzaSyBmpv5ye770qPKnwzflJAPDpttE2vXEhew")
				.body(bodyData)
				.when().post("/maps/api/place/add/xml")
				.then().assertThat().statusCode(200).and().contentType(ContentType.XML)
				.and().extract().response();
			
			
			XmlPath xml =ReusableMethod.rawToXml(res); 
			String place_id = xml.get("PlaceAddResponse.place_id");
			
			System.out.println(place_id);
			//Grab placeId
//			String resString = res.asString();
//			System.out.println(resString);
//			
//			JsonPath json = new JsonPath(resString);
//			String placeId = json.get("place_id");
//			System.out.println(placeId);
//			
//			//Delete Place
//			
//			given().queryParam("key","AIzaSyBmpv5ye770qPKnwzflJAPDpttE2vXEhew")
//			.body("{" + 
//					" \"place_id\" : \""+placeId+"\" "+
//					"}")
//			.when().post("maps/api/place/delete/json")
//			.then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
//			.and().body("status", equalTo("OK"));
					
	}

	public static String GenerateStringFromResponse(String path) throws IOException {
		return new String(Files.readAllBytes(Paths.get(path)));
	}
}
