package tests;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Basics3 {
	@Test
	public void postAnddeleteData() {
		
//		 					Task1 (add a place)
		RestAssured.baseURI = "https://maps.googleapis.com";
		String bodyData = "{\n" + 
				"  \"location\": {\n" + 
				"    \"lat\": -33.8669710,\n" + 
				"    \"lng\": 151.1958750\n" + 
				"  },\n" + 
				"  \"accuracy\": 50,\n" + 
				"  \"name\": \"Google Shoes!\",\n" + 
				"  \"phone_number\": \"(02) 9374 4000\",\n" + 
				"  \"address\": \"48 Pirrama Road, Pyrmont, NSW 2009, Australia\",\n" + 
				"  \"types\": [\"shoe_store\"],\n" + 
				"  \"website\": \"http://www.google.com.au/\",\n" + 
				"  \"language\": \"en-AU\"\n" + 
				"}"; 
		
			Response res = given().queryParam("key","AIzaSyBmpv5ye770qPKnwzflJAPDpttE2vXEhew")
				.body(bodyData)
				.when().post("/maps/api/place/add/json")
				.then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
				.and().body("status", equalTo("OK")).extract().response();
			
			
			//Grab placeId
			String resString = res.asString();
			System.out.println(resString);
			
			JsonPath json = new JsonPath(resString);
			String placeId = json.get("place_id");
			System.out.println(placeId);
			
			//Delete Place
			
			given().queryParam("key","AIzaSyBmpv5ye770qPKnwzflJAPDpttE2vXEhew")
			.body("{" + 
					" \"place_id\" : \""+placeId+"\" "+
					"}")
			.when().post("maps/api/place/delete/json")
			.then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
			.and().body("status", equalTo("OK"));
					
	}

}
