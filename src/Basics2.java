import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import static org.hamcrest.Matchers.equalTo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import files.payload;
import files.resources;

public class Basics2 {

	Properties prop = new Properties();
	
	@BeforeTest
	public void getData() throws IOException {
			FileInputStream fis = new FileInputStream("/home/rohit/Documents/Code/Eclipse/RestAssured/src/files/env.properties");
			prop.load(fis);
			prop.get("HOST");
	}
	
	@Test
	public void postData() {
		
		RestAssured.baseURI = prop.getProperty("HOST");
		
		given().queryParam("key",prop.getProperty("KEY"))
		.body(payload.payLoadData())
		.when().post(resources.getPostData())
		.then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
		.and().body("status", equalTo("OK"));
	}
}
