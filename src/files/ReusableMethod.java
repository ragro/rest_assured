package files;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

public class ReusableMethod {

	public static XmlPath rawToXml(Response res) {

		String response = res.asString();
		XmlPath xml = new XmlPath(response);
		return xml;
	}
	
	public static JsonPath rawToJson(Response res) {
		String response = res.asString();
		JsonPath json = new JsonPath(response);
		return json;
	}
	
	public static String getSessionId() {
		
		RestAssured.baseURI = "http://localhost:8080";
		Response res = given().header("Content-Type", "application/json")
				.body("{ \"username\": \"Rohit\", \"password\": \"r@hit.14/cs75\" }")
				.when().post("/rest/auth/1/session ").then()
					.statusCode(200).extract().response();
		
		JsonPath json = ReusableMethod.rawToJson(res);
		String sessionId = json.get("session.value");
		return sessionId;
		
	}
	
//	public static String getIssueId() {
//		String issueId;
//		return issueId;
//	}
//	
//	public static String getCommentId() {
//		String commentId;
//		return commentId;
//	}
}
