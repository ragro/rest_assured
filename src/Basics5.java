import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

import files.ReusableMethod;

import static io.restassured.RestAssured.given;

public class Basics5 {
	
	@Test
	public void Test(){
		RestAssured.baseURI = "https://maps.googleapis.com";
		
		Response res = 
						given().log().all().
						param("location","-33.8670522,151.1957362").
						param("radius","500").
						param("key","AIzaSyBmpv5ye770qPKnwzflJAPDpttE2vXEhew").
						when().
						get("maps/api/place/nearbysearch/json").
						then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and()
						.body("results[0].name", equalTo("Sydney")).and()
						.body("results[0].place_id", equalTo("ChIJP3Sa8ziYEmsRUKgyFmh9AQM")).log().body()
						.extract().response();
		JsonPath js = ReusableMethod.rawToJson(res);
		int size = js.get("results.size()");
		
//		" \"place_id\" : \""+placeId+"\" "+
//		"place_id" : "345678";
		
		for(int i =0; i< size; i++) {
			Object name = js.get("results["+i+"].name");
			System.out.println("Name :- "+name);
		}
		
		
	}

}
