import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import files.ReusableMethod;

import static io.restassured.RestAssured.given;



public class Basics7 {
	
	Properties prop = new Properties();
	
	@BeforeTest
	public void getData() throws IOException {
		FileInputStream fis = new FileInputStream("//home//rohit//Documents//Code//Eclipse//RestAssured//src//files//env.properties");
		prop.load(fis);
	}

	@Test
	
	public void JiraAPITest() {
		
		//creating session
		
		RestAssured.baseURI = prop.getProperty("JIRAHOST");
		
		Response res = 
						given().header("Content-Type","application/json")
						.header("Cookie","JSESSIONID="+ReusableMethod.getSessionId())
						.body(" {\n" + 
								"      \"body\": \"Hey I finally added a comment via automation.\",\n" + 
								"      \"visibility\": {\n" + 
								"        \"type\": \"role\",\n" + 
								"        \"value\": \"Administrators\"\n" + 
								"      }\n" + 
								"    }").when().post("/rest/api/2/issue/10102/comment")
									.then().extract().response();
		
		JsonPath js = ReusableMethod.rawToJson(res);
		String commentId = js.getString("id");
		
		
		res = 
				given().header("Content-Type","application/json")
				.header("Cookie","JSESSIONID="+ReusableMethod.getSessionId())
				.pathParams("commentId",commentId)
				.body(" {\n" + 
						"      \"body\": \"Hey I finally updated this comment via automation.\",\n" + 
						"      \"visibility\": {\n" + 
						"        \"type\": \"role\",\n" + 
						"        \"value\": \"Administrators\"\n" + 
						"      }\n" + 
						"    }").when().put("/rest/api/2/issue/10102/comment/{commentId}")
							.then().extract().response();
		js = ReusableMethod.rawToJson(res);
		commentId = js.getString("id");
		System.out.println("Updated Comment id :- "+commentId);
		
	}
}





















