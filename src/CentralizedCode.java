
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import files.resources;
import files.payload;
import files.ReusableMethod;

public class CentralizedCode {

	Properties prop = new Properties();
	
	@BeforeTest
	
	public void getData() throws IOException {
		FileInputStream fis = new FileInputStream("//home//rohit//Documents//Code//Eclipse//RestAssured//src//files//env.properties");
		prop.load(fis);
		prop.get("Host");
	}

	
	@Test
	public void postAnddeleteData() {
		
//		 					Task1 (add a place)
		RestAssured.baseURI = prop.getProperty("HOST");
		
		
			Response res = given().queryParam("key",prop.getProperty("KEY"))
				.body(payload.payLoadData())
				.when().post(resources.getPostData())
				.then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
				.and().body("status", equalTo("OK")).extract().response();
			
			
			//Grab placeId
			
			
			JsonPath json = ReusableMethod.rawToJson(res);
			String placeId = json.get("place_id");
			System.out.println(placeId);
			
			//Delete Place
			
			given().queryParam("key",prop.getProperty("KEY"))
			.body("{" + 
					" \"place_id\" : \""+placeId+"\" "+
					"}")
			.when().post(resources.getDelData())
			.then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
			.and().body("status", equalTo("OK"));
					
	}

}
