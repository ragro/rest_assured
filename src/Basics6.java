import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import files.ReusableMethod;

import static io.restassured.RestAssured.given;



public class Basics6 {
	
	Properties prop = new Properties();
	
	@BeforeTest
	public void getData() throws IOException {
		FileInputStream fis = new FileInputStream("//home//rohit//Documents//Code//Eclipse//RestAssured//src//files//env.properties");
		prop.load(fis);
	}

	@Test
	
	public void JiraAPITest() {
		
		//creating session
		
		RestAssured.baseURI = prop.getProperty("JIRAHOST");
		
		Response res = 
						given().header("Content-Type","application/json")
						.header("Cookie","JSESSIONID="+ReusableMethod.getSessionId())
						.body("{\n" + 
								"    \"fields\": {\n" + 
								"       \"project\":\n" + 
								"       { \n" + 
								"          \"key\": \"RES\"\n" + 
								"       },\n" + 
								"       \"summary\": \"Debit Card issue by automation\",\n" + 
								"       \"description\": \"Creating of an issue as third test\",\n" + 
								"       \"issuetype\": {\n" + 
								"          \"name\": \"Bug\"\n" + 
								"       }\n" + 
								"   }\n" + 
								"}").when().post("/rest/api/2/issue")
									.then().statusCode(201).extract().response();
		
		JsonPath js = ReusableMethod.rawToJson(res);
		
		String issueId = js.getString("id");
		System.out.println(issueId);
		
		// deleting an issue 
		given().header("Cookie","JSESSIONID="+ReusableMethod.getSessionId())
		.pathParams("issueid",issueId).
		when().delete("/rest/api/2/issue/{issueid}").then().statusCode(204).extract().response();
	}
}





















